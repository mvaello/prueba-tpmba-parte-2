const PGateway1 = require('../pgateway_1')

describe('Payment gateway 1', () => {
  let pg

  // Instantiate only once
  beforeAll(() => {
    pg = new PGateway1()
  })

  it('should ensure pay', () => {
    expect(pg.pay()).toBe('PG1 - Payment done 💸')
  })

  it('should ensure reimbuse', () => {
    expect(pg.reimburse()).toBe('PG1 - Reimburse done 👍')
  })

  it('should ensure partial reimbuse', () => {
    expect(pg.partialReimburse()).toBe('PG1 - Partial reimbursing done 😐👍')
  })
})
