const PGateway2 = require('../pgateway_2')

describe('Payment gateway 2', () => {
  let pg

  // Instantiate only once
  beforeAll(() => {
    pg = new PGateway2()
  })

  it('should ensure pay', () => {
    expect(pg.pay()).toBe('PG2 - Payment done 💸')
  })

  it('should ensure reimbuse', () => {
    expect(pg.reimburse()).toBe('PG2 - Reimburse done 👍')
  })

  it('throws on partial reimbuse (not available)', () => {
    expect(() => { pg.partialReimburse() }).toThrow(Error('PG2 - Partial reimbursing not available 😞'))
  })
})
