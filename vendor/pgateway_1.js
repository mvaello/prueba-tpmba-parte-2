const PaymentGateway = require('../pgateway')

/**
 * Payment Gateway 1
 */
class PGateway1 extends PaymentGateway {
  /**
   * PG1 pay method
   * @returns {String}
   */
  pay () {
    return 'PG1 - Payment done 💸'
  }

  /**
   * PG1 reimburse method
   * @returns {String}
   */
  reimburse () {
    return 'PG1 - Reimburse done 👍'
  }

  /**
   * PG1 Partial reimburse method
   * @returns {String}
   */
  partialReimburse () {
    return 'PG1 - Partial reimbursing done 😐👍'
  }
}

module.exports = PGateway1
