const PaymentGateway = require('../pgateway')

/**
 * Payment Gateway 2
 */
class PGateway2 extends PaymentGateway {
  /**
   * PG2 pay method
   * @returns {String}
   */
  pay () {
    return 'PG2 - Payment done 💸'
  }

  /**
   * PG2 reimburse method
   * @returns {String}
   */
  reimburse () {
    return 'PG2 - Reimburse done 👍'
  }

  /**
   * PG2 Partial reimburse method (not available)
   * @throws Partial reimbursing not available
   */
  partialReimburse () {
    throw new Error('PG2 - Partial reimbursing not available 😞')
  }
}

module.exports = PGateway2
