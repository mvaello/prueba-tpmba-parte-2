
/**
 * Simple payment gateway example
 */
class PaymentGateway {
  /**
   * Default payment method
   * @returns {String}
   */
  pay () { }

  /**
   * Default reimburse method
   * @returns {String}
   */
  reimburse () { }
}

module.exports = PaymentGateway
